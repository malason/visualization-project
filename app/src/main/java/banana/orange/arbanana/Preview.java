package banana.orange.arbanana;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.*;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.googlecode.tesseract.android.ResultIterator;
import com.googlecode.tesseract.android.TessBaseAPI;
import org.json.JSONException;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class Preview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback, TessBaseAPI.ProgressNotifier {
    SurfaceHolder mHolder;
    Camera mCamera;
    private ARView arView;
    TessBaseAPI baseAPI;
    private Handler mHandler;
    private Thread mOCRWorker;
    private BlockingQueue<byte[]> mOCRQueue;
    private Bitmap mPreviewBitmap;
    boolean shoudDo = true;
    boolean shouldDraw = true;

    Preview(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        mHolder = getHolder();
        mHolder.addCallback(this);

        String intStorageDirectory = getContext().getFilesDir().toString();
        File folder = new File(intStorageDirectory + "/" + "tessdata");
        folder.mkdirs();
        File tarfile = new File(folder.getPath() + "/" + "eng.traineddata");
        AssetManager assets = getContext().getAssets();

        try {
            InputStream in = assets.open("tessdata/eng.traineddata");
            OutputStream out = new FileOutputStream(tarfile);
            copyFile(in, out);
            in.close();
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        startOCR();
        Log.d("TAG2", "0. Target file exists: " + Boolean.toString(tarfile.exists()));
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        if (holder == null) Log.d("tag", "surface is null");
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();
            mCamera.setDisplayOrientation(90);
        } catch (IOException e) {
            Log.d("tag", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("tag", "surfaceDestroyed");
    }


    public void addLabelOnMainThread(final String s) {
        Handler mainHandler = new Handler(getContext().getMainLooper());

        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (arView == null) {
                    arView = ARView.defaultView;
                }
                arView.clearPriceLabel();
                arView.addPriceLabel(100, 100, 100, 100, s);
            }
        });
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d("tag", "surfaceChanged");
    }

    public void startOCR() {
        baseAPI = new TessBaseAPI();
        baseAPI.setDebug(true);
        final String intStorageDirectory = getContext().getFilesDir().toString();
        File tarfile = new File(intStorageDirectory + "/tessdata/eng.traineddata");
        baseAPI.init(intStorageDirectory + "/", "eng");
        baseAPI.setVariable("tessedit_char_whitelist", "1234567890,.");
        mOCRQueue = new LinkedBlockingQueue<byte[]>();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                String text = msg.getData().getString("OcrText");
                float accuracy = msg.getData().getFloat("OcrAccuracy");
                Rect rect = msg.getData().getParcelable("OcrRect");

                if (arView == null) {
                    arView = ARView.defaultView;
                }
                arView.clearPriceLabel();
                arView.addPriceLabel(0, mCamera.getParameters().getPreviewSize().height - 50, 100, 100, text);
                try {
                    assert text != null;
                    String st = text.replace(',', '.');
                    Double d = Double.parseDouble(st);

                    if (MainActivity.instance != null) {
                        try {
                            String s = MainActivity.instance.formatText(d);
                            arView.addPriceLabel((int) (rect.left * 0.8), (int) (rect.top * 0.8), rect.height(), rect.width(), s);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (NumberFormatException exception) {
                    Log.d("LOG3", "onPreviewFrame: Cant parse" + text);
                }
                Log.d("TAG5", "handleMessage: " + text + " acc: " + accuracy);
            }
        };

        mOCRWorker = new Thread(new Runnable() {
            private Object mPauseLock = new Object();

            @Override
            public void run() {
                while (true) {
                    while (!shoudDo) {
                        synchronized (this) {
                            try {
                                wait(100);
                            } catch (InterruptedException e) {
                            }
                        }
                    }

                    try {
                        byte[] imageData = mOCRQueue.take();
                        Camera.Parameters p = mCamera.getParameters();

                        mPreviewBitmap = getBitmapImageFromYUV(imageData, p.getPreviewSize().width, p.getPreviewSize().height);
                        final Matrix mtx = new Matrix();
                        mtx.preRotate(90);


                        mPreviewBitmap = Bitmap.createBitmap(mPreviewBitmap, 0, 0, mPreviewBitmap.getWidth(), mPreviewBitmap.getHeight(), mtx, false);
                        mPreviewBitmap = toGrayscale(mPreviewBitmap);

                        if (mOCRQueue.size() > 3) {
                            mOCRQueue.clear();
                        }

                        baseAPI.clear();
                        baseAPI.setImage(mPreviewBitmap);
                        float meanaccuracy = baseAPI.meanConfidence();

                        String recognized = baseAPI.getUTF8Text();

                        String recognizedText = baseAPI.getUTF8Text();
                        final ResultIterator iterator = baseAPI.getResultIterator();
                        String lastUTF8Text;

                        float lastConfidence = 0;

                        Rect lastRect;
                        Rect bestRect = null;
                        String bestString = null;
                        float bestConfidence = 0;
                        float size = 0;
                        int count = 0;
                        int level = TessBaseAPI.PageIteratorLevel.RIL_WORD;
                        iterator.begin();

                        do {
                            lastUTF8Text = iterator.getUTF8Text(level);
                            lastConfidence = iterator.confidence(level);
                            lastRect = iterator.getBoundingRect(level);

                            if (lastConfidence > meanaccuracy && size < lastRect.width()) {
                                bestConfidence = lastConfidence;
                                bestRect = lastRect;
                                bestString = lastUTF8Text;
                                size = lastRect.width();
                            }
                            count++;

                        } while (iterator.next(level));
                        if (bestString != null) {
                            updateOCR(bestString, bestConfidence, bestRect);
                        }
                    } catch (InterruptedException e) {
                        Log.d("TAG5", "mOCRThread  InterruptedException " + e);
                    }
                }
            }
        });
        mOCRWorker.start();
    }

    private void updateOCR(String text, float accuracy, Rect bounds) {
        Message m = new Message();
        Bundle b = new Bundle();
        b.putString("OcrText", text);
        b.putFloat("OcrAccuracy", accuracy);
        b.putParcelable("OcrRect", bounds);
        m.setData(b);
        mHandler.sendMessage(m);
    }

    @Override
    public void onPreviewFrame(final byte[] bytes, final Camera camera) {
        if (mOCRQueue != null && bytes != null) {
            if (mOCRQueue.size() > 1) {
                mOCRQueue.clear();
            }
            boolean addedToQueue = mOCRQueue.offer(bytes);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            shoudDo = !shoudDo;
        }
        return true;
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void onProgressValues(TessBaseAPI.ProgressValues progressValues) {
        Log.d("TAG3", "onProgressValues: " + progressValues.getPercent());
    }


    public Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
        Camera.Parameters parameters = mCamera.getParameters();
        YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuv.compressToJpeg(new Rect(0, 0, width, height), 50, out);
        byte[] bytes = out.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        return bitmap;
    }
}

class Policz extends AsyncTask<Pair<TessBaseAPI, Bitmap>, Void, TessBaseAPI> {
    @Override
    protected TessBaseAPI doInBackground(Pair<TessBaseAPI, Bitmap>... params) {
        TessBaseAPI baseAPI = params[0].first;
        Bitmap bitmap = params[0].second;
        return baseAPI;
    }

    @Override
    protected void onPostExecute(TessBaseAPI baseAPI) {
    }
}