package banana.orange.arbanana;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

public class ARView extends View {
    class PriceLabel {
        int x = 100;
        int y = 100;
        int h = 200;
        int l = 400;
        String currentMessage = "25.10 pln";

        public PriceLabel(int y, int x, int h, int l, String currentMessage) {
            this.y = y;
            this.x = x;
            this.h = h;
            this.l = l;
            this.currentMessage = currentMessage;
        }

        public void setCurrentMessage(String currentMessage) {
            this.currentMessage = currentMessage;
        }
    }

    private Canvas c;
    final float basicTextSize = 48f;
    final float widthMargin = 96f;
    List<PriceLabel> priceLabels = new LinkedList<>();

    static ARView defaultView;
    public ARView(Context c, AttributeSet attrs)  {
        super(c, attrs);
        this.c = new Canvas();
        defaultView = this;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (PriceLabel p : priceLabels) drawPriceLabel(canvas, p);
    }

    private void drawPriceLabel(Canvas canvas, PriceLabel pl) {
        Paint p = new Paint();
        p.setColor(Color.argb(222, 127, 127, 127));
        canvas.drawRect(pl.x, pl.y, pl.x + pl.l + widthMargin, pl.y + pl.h, p);
        p.setColor(Color.BLACK);
        Rect textBounds = new Rect();
        p.setTextSize(basicTextSize);
        p.getTextBounds(pl.currentMessage, 0, pl.currentMessage.length(), textBounds);
        float desiredTextSize = basicTextSize * pl.l / textBounds.right;
        p.setTextSize(desiredTextSize);
        p.getTextBounds(pl.currentMessage, 0, pl.currentMessage.length(), textBounds);
        if (textBounds.height() > pl.h)
            desiredTextSize = desiredTextSize * pl.h / textBounds.height();
        p.setTextSize(desiredTextSize);
        canvas.drawText(pl.currentMessage, pl.x + widthMargin / 2, pl.y + pl.h / 2 + desiredTextSize / 3, p);
    }

    public Canvas getCanvas() {
        return c;
    }

    public void addPriceLabel(int x, int y, int height, int length, String message) {
        priceLabels.add(new PriceLabel(y, x, height, length, message));
        invalidate();
    }

    public void clearPriceLabel() {
        priceLabels.clear();
        invalidate();
    }
}