package banana.orange.arbanana;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CurrencyManager {
    JSONObject dict = null;
    RequestDownloader requestDownloader = new RequestDownloader(this);
    private JSONLoadedListener jsonMainListener;

    public CurrencyManager(JSONLoadedListener jsonLoadedListener) {
        this.jsonMainListener = jsonLoadedListener;
    }

    public void ParseQuery() throws ExecutionException, InterruptedException {
        String url = "http://apilayer.net/api/live?access_key=2b8fc7595d639202482fba1fd19bf105&source=USD&currencies=PLN%2CGBP%2CUSD%2CEUR%2CCHF%2CCZK&format=1";
        if (dict == null)
            requestDownloader.execute(url);
    }

    public void ParseJson(JSONObject obj) throws JSONException {
        if (dict == null) {
            dict = obj.getJSONObject("quotes");
            jsonMainListener.onJsonLoaded();
        }
    }

    public double ComputeRatio(String from, String to) throws JSONException {
        if (dict != null) {
            double f = dict.getDouble("USD" + from);
            double t = dict.getDouble("USD" + to);
            return t / f;
        } else
            return -1;
    }

    public List<String> GetAllCurrencies() {
        List<String> list = new ArrayList<String>();
        if (dict != null) {
            Iterator<?> keys = dict.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                int index = 3;
                while (index < key.length()) {
                    list.add(key.substring(index, Math.min(index + 3, key.length())));
                    index += 3;
                }
            }
            return list;
        } else
            return null;
    }
}
