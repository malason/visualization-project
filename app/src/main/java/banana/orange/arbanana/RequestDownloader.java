package banana.orange.arbanana;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestDownloader extends AsyncTask<String, Void, JSONObject> {
    CurrencyManager currencyManager;

    public RequestDownloader(CurrencyManager currencyManager) {
        this.currencyManager = currencyManager;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(String... urls) {
        try {
            return new JSONObject(downloadUrl(urls[0]));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if (result != null) {
            try {
                currencyManager.ParseJson(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String downloadUrl(String myUrl) throws IOException {
        InputStream is = null;
        int msgLen = 5000;
        try {
            URL url = new URL(myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //Miliseconds
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            //Start query
            conn.connect();
            int response = conn.getResponseCode();
            is = conn.getInputStream();
            //Convert the InputStream into a string
            String contentAsString = readIt(is, msgLen);
            return contentAsString;

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        int i;
        char c;
        StringBuilder sb = new StringBuilder();
        while ((i = reader.read()) != -1) {
            c = (char) i;
            sb.append(c);
        }
        reader.close();
        return sb.toString();
    }
}
