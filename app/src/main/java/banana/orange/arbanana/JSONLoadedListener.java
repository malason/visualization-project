package banana.orange.arbanana;

import org.json.JSONException;

public interface JSONLoadedListener {
    void onJsonLoaded() throws JSONException;
}
